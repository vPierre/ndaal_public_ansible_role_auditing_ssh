# SSH-Audit Ansible Role

This ansible role installs the pip package ssh-audit, copies a custom policy on the target based on the 
defaults selected in the role, and then runs the policy tests for the host. If the policy tests pass, 
the role completes successfully, else the assertion task will make the role fail and output the 
corresponding failure message.

## Supported Operating Systems

| Platform  | Versions             |
| --------- | -------------------- |
| EL        | 7, 8, 9              |
| Debian    | buster, bullseye, bookworm     |


## Requirements

None.

## Role Variables

The role has the following default variables that can modified [here](./defaults/main.yml):

Variable                            | Description
----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ssh_audit_port          | The port for ssh-audit. Default vaule is set to port 22.
ssh_policy_version      | The location of the created policy file.
ssh_macs                | The MACs that must match exactly (order matters).
ssh_ciphers             | The ciphers that must match exactly (order matters).
ssh_kex                 | The key exchange algorithms that must match exactly (order matters).
ssh_compression         | The compression options that must match exactly (order matters).
ssh_host_key_sizes      | Dictionary containing all host key and size information.  Optionally contains the certificate authority's signature algorithm ('ca_key_type') and signature length ('ca_key_size'), if any.
ssh_host_key_algorithms | The host key types that must match exactly (order matters).
ssh_dh_modulus_sizes    | Group exchange DH modulus sizes

The following default values are set by the role:

```yaml
---
# defaults file for SSH_Audit
ssh_audit_port: 22

# Policy template defaults
ssh_policy_version: 1
ssh_macs: "hmac-sha2-512-etm@openssh.com"
ssh_ciphers: "chacha20-poly1305@openssh.com, aes256-gcm@openssh.com, aes256-ctr"
ssh_kex: "curve25519-sha256@libssh.org"
ssh_compression: none
ssh_host_key_sizes: '{"ssh-rsa": {"hostkey_size": 4096}, "rsa-sha2-256": {"hostkey_size": 4096}, "rsa-sha2-512": {"hostkey_size": 4096}, "ssh-ed25519": {"hostkey_size": 256}}'
ssh_host_key_algorithms: "ssh-ed25519"
ssh_dh_modulus_sizes: '{"diffie-hellman-group-exchange-sha256": 2048}'
```

The port for ssh audit is set as 22 by default. 

All other variables refer to the audit standards/benchmark to perform the audit against. 
These variables are used to populate the custom policy file that can be found in [templates](./templates/policy.txt.j2).

## Custom Policy File

The following custom policy file [template](./templates/policy.txt.j2) is used by this ansible role. The jinja2 template 
sets the default variables defined in [file](./defaults/main.yml) to create the custom policy for the hosts. Therefore, 
it is essential to carefully pick the default variables. If any audit policy fails, the role will fail too. The failure 
message will be displayed showing further details. 

```
#
# Custom policy based on localhost (created on {{ current_date }})
#

# The name of this policy (displayed in the output during scans). Must be in quotes.
name = "Custom SSH Audit Policy"

# The version of this policy (displayed in the output during scans). Not parsed, and may be any value, including strings.
version = {{ ssh_policy_version }}

# The banner that must match exactly. Commented out to ignore banners, since minor variability in the banner is sometimes normal.
# banner = "SSH-2.0-OpenSSH_8.4p1 Debian-5+deb11u1"

# The compression options that must match exactly (order matters).
compressions = {{ ssh_compression }}

# Dictionary containing all host key and size information. Optionally contains the certificate authority's signature algorithm ('ca_key_type') and signature length ('ca_key_size'), if any.
host_key_sizes = {{ ssh_host_key_sizes }}

# Group exchange DH modulus sizes.
dh_modulus_sizes = {{ ssh_dh_modulus_sizes }}

# The host key types that must match exactly (order matters).
host keys = {{ ssh_host_key_algorithms }}

# Host key types that may optionally appear.
#optional host keys = ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com

# The key exchange algorithms that must match exactly (order matters).
key exchanges = {{ ssh_kex }}

# The ciphers that must match exactly (order matters).
ciphers = {{ ssh_ciphers }}

# The MACs that must match exactly (order matters).
macs = {{ ssh_macs }}

```

## Dependencies

The role has no dependencies. However, it needs sufficient privileges to install the apt and pip packages. 
The relevant dependencies of the ssh-audit pip package are installed by the role itself.

## Example Playbook

``` yaml
- hosts: servers
  become: true
  become_method: sudo
  roles:
    - SSH_Audit
```

## References:

- https://github.com/jtesta/ssh-audit
- https://pypi.org/project/ssh-audit/

## Contribution

Any contribution to improve the role is welcome.

## Author

- Pierre Gronau (vPierre) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne
- Ayesha Shafqat / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne
